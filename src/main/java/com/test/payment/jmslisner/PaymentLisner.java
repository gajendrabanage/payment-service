package com.test.payment.jmslisner;

import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import com.test.jms.PaymentRequestDTO;
import com.test.jms.PaymentResponseDTO;
import com.test.payment.entity.Payment;
import com.test.payment.repository.PaymentRepository;

@Component
public class PaymentLisner {

	@Autowired
	PaymentRepository paymentRepository;
	
	
	@Autowired
	private JmsMessagingTemplate jmsMessagingTemplate;
	
	@JmsListener(destination = "payment", containerFactory = "queueConnectionFactory")
	public void receiveQueue(PaymentRequestDTO paymentRequestDTO) {
			
		Payment payment = new Payment(); 
		BeanUtils.copyProperties(paymentRequestDTO, payment);
		payment.setStatus("DONE");
		Payment savedPayment = paymentRepository.save(payment);
		
		this.jmsMessagingTemplate.convertAndSend(new ActiveMQTopic("paymentResponse"),
				new PaymentResponseDTO(savedPayment.getTicketId(), savedPayment.getStatus()));
		
		System.out.println(savedPayment); 
	}
}
